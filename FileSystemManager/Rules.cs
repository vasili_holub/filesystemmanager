﻿using System.Configuration;

namespace FileSystemManager
{
    public class Rules: ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new Rule();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((Rule)element).Name;
        }
    }
}
