﻿using System.Configuration;

namespace FileSystemManager
{
    public class LocalizationSection : ConfigurationSection
    {
        [ConfigurationProperty("appName")]
        public string ApplicationName => (string)base["appName"];

        [ConfigurationProperty("localization")] 
        public Localization Localization => (Localization)this["localization"];

        [ConfigurationProperty("watchedDirectories")]
        public WatchedDirectories WatchedDirectories => (WatchedDirectories)this["watchedDirectories"];

        [ConfigurationProperty("rules")]
        public Rules Rules => (Rules)this["rules"];
    }
}
