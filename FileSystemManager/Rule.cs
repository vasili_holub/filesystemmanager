﻿using System.Configuration;

namespace FileSystemManager
{
    public class Rule : ConfigurationElement
    {
        [ConfigurationProperty("name", IsKey = true)]
        public string Name => (string)base["name"];

        [ConfigurationProperty("pattern")]
        public string Pattern => (string)base["pattern"];

        [ConfigurationProperty("directory")]
        public string Directory => (string)base["directory"];

        [ConfigurationProperty("enumeration")]
        public bool Enumeration => (bool)base["enumeration"];

        [ConfigurationProperty("addCreationTime")]
        public bool AddCreationTime => (bool)base["addCreationTime"];
    }
}
