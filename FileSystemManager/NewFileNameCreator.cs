﻿namespace FileSystemManager
{
    using System;
    using System.IO;
    using System.Linq;

    public class NewFileNameCreator
    {
        private readonly string _oldFullPath;
        private readonly string _directoryDestination;
        private readonly bool _isApplyEnumeration = false;
        private readonly bool _isApplyTimeStampInfo = false;
        private readonly string _dateTimeStamp;

        public bool NoChangesNeeded => !_isApplyEnumeration && !_isApplyTimeStampInfo;

        public NewFileNameCreator(string oldFullPath, string directoryDestination, string dateTimeStamp, bool isApplyEnumeration, bool isApplyTimeStampInfo)
        {
            _oldFullPath = oldFullPath;
            _directoryDestination = directoryDestination;
            _isApplyEnumeration = isApplyEnumeration;
            _isApplyTimeStampInfo = isApplyTimeStampInfo;
            _dateTimeStamp = dateTimeStamp;
        }

        public string Create()
        {
            if (NoChangesNeeded)
            {
                return _oldFullPath;
            }

            var fileNameDecorator = new FileNameDecorator(GetPrefix(), GetPostFix());

            return fileNameDecorator.Decorate(_oldFullPath);
        }

        public string GetPrefix()
        {
            if (!Directory.Exists(_directoryDestination))
            {
                return string.Empty;
            }

            var directory = new DirectoryInfo(_directoryDestination);

            var filesCount = directory.GetFiles().Length;

            var nextCountString = (filesCount + 1).ToString();

            return nextCountString.PadLeft(6, Convert.ToChar("0")) + "_";
        }

        public string GetPostFix()
        {
            if (string.IsNullOrEmpty(_dateTimeStamp))
            {
                return string.Empty;
            }

            var stringsForReplacement = new[] {" ", "/", ":", "."};

            return "_" + stringsForReplacement.Aggregate(_dateTimeStamp, (current, stringForReplacement) => current.Replace(stringForReplacement, "_"));
        }
    }
}
