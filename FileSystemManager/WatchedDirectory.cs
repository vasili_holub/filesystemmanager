﻿using System.Configuration;

namespace FileSystemManager
{
    public class WatchedDirectory: ConfigurationElement
    {
        [ConfigurationProperty("name", IsKey = true)]
        public string Name => (string)base["name"];
    }
}
