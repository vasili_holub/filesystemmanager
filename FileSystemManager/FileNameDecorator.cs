﻿namespace FileSystemManager
{
    using System.IO;

    public class FileNameDecorator
    {
        private readonly string _prefix;
        private readonly string _postfix;

        public FileNameDecorator(string prefix, string postfix)
        {
            _prefix = prefix;
            _postfix = postfix;
        }

        public string Decorate(string fullPath)
        {
            var directoryName = Path.GetDirectoryName(fullPath);
            var fileExtension = Path.GetExtension(fullPath);
            var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(fullPath);

            var fileNameWithoutExtensionDecorated =
                (_prefix ?? string.Empty) + fileNameWithoutExtension + (_postfix ?? string.Empty);

            var fileNameDecorated = fileNameWithoutExtensionDecorated + fileExtension;

            return Path.Combine(directoryName, fileNameDecorated);
        }
    }
}
