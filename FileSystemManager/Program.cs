﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using Messages = FileSystemManager.Properties.Resources;

namespace FileSystemManager
{
    public class Program
    {
        private static LocalizationSection _configuration;
        private static string _language;
        private static Rules _rules;
        private static List<FileSystemManager> _fileSystemManagers;
        public static void Main(string[] args)
        {
            Setup();

            InformAboutWorkStarted();

            InformAboutProgramAbort();

            StartManaging();

            Console.ReadKey();

            StopManaging();

            InformAboutWorkFinished();
        }

        private static void Setup()
        {
            _configuration = (LocalizationSection)ConfigurationManager.GetSection("localizationSection");
            _language = _configuration.Localization.Language;
            _rules = _configuration.Rules;
            _fileSystemManagers = new List<FileSystemManager>();
        }

        private static void StartManaging()
        {
            foreach (var directory in _configuration.WatchedDirectories)
            {
                var watchedDirectory = directory as WatchedDirectory;
                var fileSystemWatcher = new FileSystemWatcher(watchedDirectory.Name)
                {
                    //fileSystemWatcher.NotifyFilter = NotifyFilters.LastWrite;
                    EnableRaisingEvents = true
                };
                var fileSystemManager = new FileSystemManager(fileSystemWatcher, _rules, _language);
                _fileSystemManagers.Add(fileSystemManager);
            }
        }

        private static void StopManaging()
        {
            // Unsubscribes from FileSystemWatcher events.
            foreach (var fileSystemManager in _fileSystemManagers)
            {
                fileSystemManager.Dispose();
            }
        }

        private static void InformAboutProgramAbort()
        {
            switch (_language)
            {
                case "ru":
                    Console.WriteLine(Messages._002_003_interface_info_about_abort_program_ru);
                    break;
                case "en":
                    Console.WriteLine(Messages._002_003_interface_info_about_abort_program_en);
                    break;
            }
        }

        private static void InformAboutWorkStarted()
        {
            switch (_language)
            {
                case "ru":
                    Console.WriteLine(Messages._002_001_interface_start_ru);
                    break;
                case "en":
                    Console.WriteLine(Messages._002_001_interface_start_en);
                    break;
            }
        }

        private static void InformAboutWorkFinished()
        {
            switch (_language)
            {
                case "ru":
                    Console.WriteLine(Messages._002_002_interface_finish_ru);
                    break;
                case "en":
                    Console.WriteLine(Messages._002_002_interface_finish_en);
                    break;
            }
        }
    }
}
