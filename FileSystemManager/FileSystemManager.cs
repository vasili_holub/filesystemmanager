﻿using System;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;
using Messages = FileSystemManager.Properties.Resources;

namespace FileSystemManager
{
    public class FileSystemManager: IDisposable
    {
        private const string DefaultRuleName = "default";

        private readonly FileSystemWatcher _fleSystemWatcher;

        private readonly CultureInfo _culture;

        private readonly Rules _rules;

        private FileInfo _processingFile;

        private readonly string _language;
        public FileSystemManager(FileSystemWatcher fileSystemWatcher, Rules rules, string language)
        {
            _fleSystemWatcher = fileSystemWatcher;
            _rules = rules;
            _language = language;
            _culture = new CultureInfo($"{_language.ToLower()}-{_language.ToUpper()}");
            _fleSystemWatcher.Created += FileSystemWatcher_Created;
        }

        public void Dispose()
        {
            _fleSystemWatcher.Created -= FileSystemWatcher_Created;
        }

        private void FileSystemWatcher_Created(object sender, FileSystemEventArgs e)
        {
            _processingFile = new FileInfo(e.FullPath);

            InformAboutFileFound(_processingFile);

            var rule = GetSuitableRule(Path.GetFileName(e.FullPath));

            ApplyRule(rule, e.FullPath);
        }

        private void ApplyRule(Rule rule, string fullPath)
        {
            InformAboutMovingFile(_processingFile, rule.Directory);
            var now = DateTime.Now;
            var newFileNameCreator = new NewFileNameCreator(fullPath, rule.Directory, now.ToString(_culture), rule.Enumeration, rule.AddCreationTime);
            var newFullPath = newFileNameCreator.Create();
            File.Move(fullPath, Path.Combine(rule.Directory, Path.GetFileName(newFullPath)), true);
        }

        private Rule GetSuitableRule(string fileName)
        {
            foreach (var rule in _rules)
            {
                var localRule = rule as Rule;
                if (localRule.Pattern == DefaultRuleName)
                {
                    continue;
                }
                var match = Regex.Match(fileName, localRule.Pattern, RegexOptions.IgnoreCase);
                if (match.Success)
                {
                    InformAboutSuitableRuleFound(fileName);
                    return localRule;
                }
            }

            foreach (var rule in _rules)
            {
                var localRule = rule as Rule;
                if (localRule.Name == DefaultRuleName)
                {
                    return localRule;
                }
            }

            throw new NotImplementedException();
        }

        private void InformAboutFileFound(FileSystemInfo file)
        {
            switch (_language)
            {
                case "ru":
                    Console.WriteLine(string.Format(Messages._001_001_message_file_found_ru, file.Name, file.CreationTime.ToString(_culture), Path.GetDirectoryName(file.FullName)));
                    break;
                case "en":
                    Console.WriteLine(string.Format(Messages._001_001_message_file_found_en, file.Name, file.CreationTime.ToString(_culture), Path.GetDirectoryName(file.FullName)));
                    break;
            }
        }

        private void InformAboutSuitableRuleFound(string fullPath)
        {
            switch (_language)
            {
                case "ru":
                    Console.WriteLine(string.Format(Messages._001_002_message_suitable_rule_was_found_ru, fullPath));
                    break;
                case "en":
                    Console.WriteLine(string.Format(Messages._001_002_message_suitable_rule_was_found_en, fullPath));
                    break;
            }
        }

        private void InformAboutMovingFile(FileSystemInfo file, string destinationDirectory)
        {
            var now = DateTime.Now;
            switch (_language)
            {
                case "ru":
                    Console.WriteLine(string.Format(Messages._001_003_message_file_was_move_to_folder_ru, file.FullName, file.CreationTime.ToString(_culture), destinationDirectory, now.ToString(_culture)));
                    break;
                case "en":
                    Console.WriteLine(string.Format(Messages._001_003_message_file_was_move_to_folder_en, file.FullName, file.CreationTime.ToString(_culture), destinationDirectory, now.ToString(_culture)));
                    break;
            }
        }

    }
}
