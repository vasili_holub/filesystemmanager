﻿using System.Configuration;

namespace FileSystemManager
{
    public class Localization: ConfigurationElement
    {
        [ConfigurationProperty("language")]
        public string Language => (string)this["language"];
    }
}
