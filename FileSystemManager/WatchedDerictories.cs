﻿using System.Configuration;

namespace FileSystemManager
{
    public class WatchedDirectories: ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new WatchedDirectory();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((WatchedDirectory)element).Name;
        }
    }
}
