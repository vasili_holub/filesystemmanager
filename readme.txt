For testing program please uncompress file FileSystemManager.rar into the C:\Training directories.

1. Directories, which are being watched:

C:\Training\FileSystemManager\Test1
C:\Training\FileSystemManager\Test2
C:\Training\FileSystemManager\Test3
C:\Training\FileSystemManager\Test4
C:\Training\FileSystemManager\Test5

2. Templates files are presented in the following directory:

C:\Training\FileSystemManager\Files templates

3. Files are moved in the following directories according to the established rules:

C:\Training\FileSystemManager\Destination\Test6 (*.txt files)
C:\Training\FileSystemManager\Destination\Test7 (*.doc files)
C:\Training\FileSystemManager\Destination\Test8 (*test*.* files)
C:\Training\FileSystemManager\Destination\Test9 (*Mapping*.* files)
C:\Training\FileSystemManager\Destination\Test10 (all the other files).

4. To see how manager moves file to the different locations, take one by one files from the
C:\Training\FileSystemManager\Files templates directory and put them in one of the directories 
indicated in the point 1. of this readme file.